#!/usr/bin/env python3
import os, ctypes, sys, configparser

class Setup():

    def __init__(self):
        self.config = configparser.ConfigParser()
        self.config.read('settings.ini')
        if 'DEFAULT' not in self.config or os.path.isfile('settings.ini') is False:

            self.config['DEFAULT'] = {'Players': '1',
                             'Enemies': '5',
                             'Score': '0'}
            with open('settings.ini', 'w') as configFile:
                self.config.write(configFile)
                configFile.close()

            self.player = self.config['DEFAULT']['Players']
            self.enemies = self.config['DEFAULT']['Enemies']
            self.turn = 0
            self.score = self.config['DEFAULT']['Score']
            self.forwards = 0
            self.backwards = 0
            self.right = 0
            self.left = 0
        elif 'USER' in self.config:
            self.player = self.config['USER']['Players']
            self.enemies = self.config['USER']['Enemies']
            self.turn = 0
            self.score = self.config['USER']['Score']
            self.forwards = 0
            self.backwards = 0
            self.right = 0
            self.left = 0
        else:
            self.player = self.config['DEFAULT']['Players']
            self.enemies = self.config['DEFAULT']['Enemies']
            self.turn = 0
            self.score = self.config['DEFAULT']['Score']
            self.forwards = 0
            self.backwards = 0
            self.right = 0
            self.left = 0
    def checker(self):
        self.clear_screen()
        print('\nPlease be adviced this content is for Mature audience\'s only. If you are no 16+ I am not responsible if you accept these terms.\n')
        check=input('Do you accept? y/n: ')
        if check.lower() == 'y':
            return
        else:
            exit(0)
    def _continue(self):
        input('Press ENTER to continue...')
        return

    def _high_score(self):
        with open('hs', 'r') as high_score:
            high_score.read()
            high_score.close()
    
    def _options(self):
        options=f'''\n\nPlayers: {self.player}\nEnemies: {self.enemies}\nScore:   {self.score}'''
        print(options)
        change=input('\nWould you like to change anything? y/n: ')

        if change.lower() == 'y':
            option=input('Which option?(spaces in between numbers for multiple): ')
            option=option.lower()
            list_options = []
            list_options=option.split(' ')

            for i, opt in enumerate(list_options):
                # Change amount of players (1 or 2)
                if opt == 'players':
                    players=input('How many players do I want to play with?: ')

                    while int(players) < 1 or int(players) > 2:
                        print('\n\nEither you\'re very alone, or you have a lot of friends with you right now.\nPlease just try 1 or 2 players.')
                        players=input('\n\nHow many players do you want to play with?: ')
                                        
                    if 'USER' not in self.config:
                        self.config['USER']={'Players': f'{players}', 'Enemies': f'{self.enemies}', 'Score': f'{self.score}'}
                        self.player = self.config['USER']['Players']
                        with open('settings.ini', 'w') as configFile:
                            self.config.write(configFile)
                            configFile.close()
                    else:
                        self.config['USER']={'Players': f'{players}', 'Enemies': f'{self.enemies}', 'Score': f'{self.score}'}
                        self.player = self.config['USER']['Players']
                        with open('settings.ini', 'w') as configFile:
                            self.config.write(configFile)
                            configFile.close()
                # Change amount of enemies    
                elif opt == 'enemies':
                    enemies=input('How many enemies do I want to have?: (5 easiest - 100 hardest): ')

                    while int(enemies) < 5 or int(enemies) > 100:
                        print('Stop trying to break things! You hethan! Choose a number from 5-100')
                        enemies=input('How many enemies do you want to have?: (5 easiest - 100 hardest): ')

                    if 'USER' not in self.config:
                        self.config['USER']={'Players': f'{self.player}', 'Enemies': f'{enemies}', 'Score': f'{self.score}'}
                        self.enemies = self.config['USER']['Enemies']
                        with open('settings.ini', 'w') as configFile:
                            self.config.write(configFile)
                            configFile.close()
                    else:
                        self.config['USER']={'Players': f'{self.player}', 'Enemies': f'{enemies}', 'Score': f'{self.score}'}
                        self.enemies = self.config['USER']['Enemies']
                        with open('settings.ini', 'w') as configFile:
                            self.config.write(configFile)
                            configFile.close()
                # Score Reset    
                elif opt == 'score':
                    score=input('Reset High Score? y/n: ')
                    score=score.lower()

                    while score != "y" and score != "n":
                        print('I really think I\'m that dumb huh? Enter \'y\' or \'n\'')
                        score=''
                        score=input('Reset High Score? y/n: ')
                    if score == "y":
                        if 'USER' not in self.config:
                            self.config['USER']={'Players': f'{self.player}', 'Enemies': f'{self.enemies}', 'Score': f'{self.score}'}
                            self.score = self.config['USER']['Score']
                            with open('settings.ini', 'w') as configFile:
                                self.config.write(configFile)
                                configFile.close()
                        else:
                            self.config['USER']={'Players': f'{self.player}', 'Enemies': f'{self.enemies}', 'Score': f'{self.score}'}
                            self.score = self.config['USER']['Score']
                            with open('settings.ini', 'w') as configFile:
                                self.config.write(configFile)
                                configFile.close()
                        
                    else:
                        self.main_menu()
                if i > 3 or len(list_options) == 1:
                        self.main_menu()
            self.main_menu()
        else:
            self.main_menu()

    def clear_screen(self):
        if sys.platform.lower() == 'linux' or sys.platform.lower() == 'linux2' or sys.platform.lower() == 'darwin':
            os.system('clear')
        elif sys.platform.lower() == 'nt' or sys.platform.lower() == 'win32' or sys.platform.lower() == 'win64':
            os.system('cls')
        else:
            print('Usupported Device')
            exit(1)
        return

    def _house(self):
        print('house')

    def _go_right(self):
        self.clear_screen()
        right_turn='''    I decided to quickly turn right, going into the ditch, but still alive and well. 
"Shit that was close", I said. "Where the fuck did that truck come from?"
I try to start the car, but it just kept cranking over. Smoke was pouring out of the fucked up hood which 
previously looked prestine. I see what looks like a house ahead of me. I can barly make out the light from the windows
in this downpouring rain though. I looked behind me, seeing that car lights that once before were faint were 
getting closer, but still distant. "Fuck I need to call someone, 
but my phone has no service right now. I need to see if I can get help"...\n'''
        print(right_turn)
        self._continue()
        self.clear_screen()
        userInput=input('\n############################\n#      Enter and option    #\n#                          #\n# 1.) Get out of car and   #\n#  walk towards car coming #\n# from behind              #\n#                          #\n# 2.) Get out of car and   #\n# walk towards what looks  #\n# to be a                  #\n# house in the near        #\n# distance                 #\n#                          #\n############################\nChoice: ')
        if userInput == '1':
            print('')
        elif userInput == '2':
            self._house()
        else:
            self._go_right()
    
    def _go_left(self):
        self.clear_screen()
        print('')

    def _drive_on(self):
        self.clear_screen()
        driving='''\n    I decided to fight the restless eyes that where heavy upon myself. "I can do this.", I say to myself. 
I look at the radio again. "Fucking damn thing why don\'t you work!" I see a bright light, I look up and there is a
18 wheeler headed right for me!'''
        print(driving)
        self._continue()
        self.clear_screen()
        userInput=input('\n############################\n#      Enter and option    #\n#                          #\n# 1.) Turn Right           #\n# 2.) Turn Left            #\n#                          #\n############################\nChoice: ')

        if userInput == '1':
            self._go_right()
        else:
            self._go_left()
    def _pull_over(self):
        print()
    
    def _run(self):
        # TODO: Build game
        self.clear_screen()
        intro = '''                         -Chapter One-\n\n        It all started when I was on route 104 in the middle of nowhere.\nIt was raining and I was tired as shit.
I've been up for 3 days straight trying to find out what happned to Mrs. Brown.\nShe was slaughterd by someone four nights ago.
Murdered and then attached to a clothes hanger like she was fucking \ndirty laundry hung out to dry.\n
        My eyes were heavy... "Shit, I need to get some place to get out off this \nfucking rain", I said. By the way my name is Detective Jones.
I have been on the police force here in New Mexico for about 12 years coming up \nin October of this year. The year is 1970,
everyone is just getting settled down from the so called, "Best year of peoples \nlives" of \'69. I think every year is a shit hole waiting
to spread. But 1970 has to be the worst year have seen in my life, as of yet.\n
        It\'s been about 2 hours since I have last seen a car pass me. Although \nin the distance behind me a can see what looks to be car lights in the faint
distance. My radio is getting nothing but static for stations out here. \n"I must stay awake!", I said to myself....\n'''
        print(intro)
        self._continue()
        self.clear_screen()
        userInput = input('\n############################\n#      Enter and option    #\n#                          #\n# 1.) Keep Driving         #\n# 2.) Pull over and sleep  #\n############################\nChoice: ')
        if userInput == '1':
            self._drive_on()
        else:
            self._pull_over()

    def main_menu(self):
        self.clear_screen()
        print("Hello!, Welcome to Can I escape? A turn-by-turn stratagy game\n\n"+
        "Please choose an option below to start the adventure!\n\n")
        _main_menu_options = '''    #################################################
    #                                               #
    # 1.) Start                  2.) High Score     #
    #                                               #
    # 3.) Options                4.) Exit           #
    #                                               #
    #################################################\n'''  
        print(_main_menu_options)      
        userInput = input('Choice: ')

        while int(userInput) < 1 or int(userInput) > 4:
            print('Improper selection. Please re-enter a number between 1 and 4.\n')
            userInput = input('Choice: ')
        
        if userInput == '1':
            self._run()
        elif userInput == '2':
            self._high_score()
        elif userInput == '3':
            self._options()
        else:
            print("\nThanks for playing!\n")
            exit(0)

def main():
    game = Setup()
    game.checker()
    game.main_menu()
    
def __init__():
    try:
    
        if os.geteuid() == 0:
            print("\nDon't run this as root please.\n")
            exit(1)
        else:
            main()
    except AttributeError:
        if ctypes.windll.shell32.IsUserAnAdmin() == 0:
            print("\nDon't run this as Administrator please.\n")
            exit(1)
        else:
            main()

if __name__ == '__main__':
    __init__()
